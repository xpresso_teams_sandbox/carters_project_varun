"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
import numpy as np
import tensorflow as tf
import os
from pdf2image import convert_from_path, convert_from_bytes

from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)
# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="infer",
                   level=logging.INFO)


class Infer(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """
        self.model1 = None
        self.model2 = None
        self.model3 = None
        self.model4 = None
        self.model5 = None
        
    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        
        self.model = tf.keras.models.load_model(os.path.join(model_path, "leg_length.h5"))
        self.model1 = tf.keras.models.load_model(os.path.join(model_path, "sleeve_length.h5"))
        self.model2 = tf.keras.models.load_model(os.path.join(model_path, "leg_type.h5"))
        self.model3 = tf.keras.models.load_model(os.path.join(model_path, "sleeve_type.h5"))
        self.model4 = tf.keras.models.load_model(os.path.join(model_path, "character.h5"))
        self.model5 = tf.keras.models.load_model(os.path.join(model_path, "pattern.h5"))
        pass

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        
        #import PDF or PNG, convert to array, normalize array
        try:
            image = convert_from_bytes(open(input_request, 'rb').read(), size = (224, 224))
            x_o = tf.keras.preprocessing.image.img_to_array(image[0], data_format = "channels_last")
            
        except PDFPageCountError:
            image = tf.keras.preprocessing.image.load_img(input_request, target_size = (224,224))
            x_o = tf.keras.preprocessing.image.img_to_array(image, data_format = "channels_last")
        except ValueError:
            image = tf.keras.preprocessing.image.load_img(input_request, target_size = (224,224))
            x_o = tf.keras.preprocessing.image.img_to_array(image, data_format = "channels_last")
            
        X = x_o / 255
        
        return X
        

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        models = [self.model, self.model1, self.model2, self.model3, self.model4, self.model5]
        attributes = [['leglong', 0, 'legshort'], ['sleevelong', 0, 'sleeveshort', 'sleeveless'], [0, 'cinched', 
                        'jogger', 'legging', 'short', 'footed', 'jegging','skirt'], [0, 'rufflesleeve', 'raglan', 'cap'],
                        ['dinosaur', 'flowers', 'heart', 'car', 'football', 'airplane', 'cloud', 'alphabet', 'alligator', 'cheetah', 'giraffe', 'deer',
                            'cupcake', 'gifts', 'astronaut', 'flamingo', 'ballet'], ['print', 'floral', 'stripe', 'solid', 'wordprint', 'camouflage', 'colorblock']]
        y_hat = []
        for i in range(len(models)):
            class_s = np.argmax(tf.nn.softmax(models[i](input_request.reshape(1, 224, 224, 3))))
            label = attributes[i][class_s]
            y_hat.append(label)

        return y_hat

    def transform_output(self, y_hat):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        labels = {"Primary Leg Length": y_hat[0],
                  "Primary Sleeve Length": y_hat[1],
                  "Primary Leg Type": y_hat[2],
                  "Primary Sleeve Type": y_hat[3],
                  "Primary Character": y_hat[4],
                  "Print Pattern": y_hat[5]
                  }
        return labels
        


if __name__ == "__main__":
    pred = Infer()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
