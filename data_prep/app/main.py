"""
This is the implementation of data preparation for sklearn
"""

import sys
import os
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

from xpresso.ai.core.data.automl.structured_dataset import StructuredDataset
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.client.controller_client import ControllerClient

import pandas as pd
import numpy as np
import tensorflow as tf

from pdf2image import convert_from_path, convert_from_bytes

from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)

__author__ = "### Author ###"

logger = XprLogger("data_prep",level=logging.INFO)


class DataPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="DataPrep")
        """ Initialize all the required constansts and data her """
        self.X = None
        self.ydf = None
        self.style_id = None
    def get_img_dataset(self):
        version_controller_factory = VersionControllerFactory()
        version_controller = version_controller_factory.get_version_controller()
            
        pull_info = {
            "repo_name" : "carters_project_varun",
            "branch_name" : "deploy",
            "path": "/dataset/images",
            "save_at": "/data/X",
            "output_type": "files"
        }
            
        version_controller.pull_dataset(**pull_info)
        
        X_path = "/data/X/dataset/images"
        return X_path
        
    def get_styleID(self):
        version_controller_factory = VersionControllerFactory()
        version_controller = version_controller_factory.get_version_controller()
            
        pull_info = {
            "repo_name" : "carters_project_varun",
            "branch_name" : "deploy",
            "path": "/dataset/labels",
            "save_at": "/data/labels",
            "output_type": "files"
        }
            
        version_controller.pull_dataset(**pull_info)
        
        df = pd.read_excel('/data/labels/dataset/labels/carters_data.xlsx', skiprows = 0, header = 1, index_col = None)
        style_id = df["Style ID"]
        self.style_id = style_id
        return style_id
        
    def get_num(self, value):
        num = value.split('.')[0]
        ind = self.style_id[self.style_id == num].index[0]
        return ind
        
    def generate_X(self, X_path, m):
        directory = X_path
        X = np.zeros((m, 224, 224, 3))
        i = 0
        for filename in sorted(os.listdir(directory), key = self.get_num):
            try:
                img = convert_from_path(os.path.join(directory, filename), size = (224, 224))
                x_i = tf.keras.preprocessing.image.img_to_array(img[0], data_format = "channels_last")
            except PDFPageCountError:
                img = tf.keras.preprocessing.image.load_img(os.path.join(directory, filename), target_size = (224,224))
                x_i = tf.keras.preprocessing.image.img_to_array(img, data_format = "channels_last")
            except ValueError:
                img = tf.keras.preprocessing.image.load_img(os.path.join(directory, filename), target_size = (224,224))
                x_i = tf.keras.preprocessing.image.img_to_array(img, data_format = "channels_last")
    
            X[i] = x_i
            i+=1
            if i % 20 == 0:
                print(x_i.shape)

        print(X.shape)
        
        return X
    
    
    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            #log in to xpresso
            Cc = ControllerClient()
            Cc.login("varun.kumar", "Varun@123")
            
            logging.info("Logged in")
            
            #pull dataset from data versioning system
            X_path = self.get_img_dataset()
            style_id = self.get_styleID()
            m = len(style_id)
            self.X = self.generate_X(X_path, m)
            
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        
        np.save('/data/X_raw.npy', self.X)
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = DataPrep()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
