"""
TODO:

- Address skewed data distribution by oversampling minority classes
- Use generators while training to optimize memory
- Use checkpointing and pause/resume functionality
- Train more attributes
- Use regularization (if necessary with more data)

"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

MOUNT_PATH = '/data'
import os
import logging
import types
import pickle
import marshal
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.client.controller_client import ControllerClient

__author__ = "### Author ###"

logger = XprLogger("train",level=logging.INFO)


class Train(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="Train")
        """ Initialize all the required constansts and data her """

    def encode(self, sr):
        ''' (Series) --> np.array
        Encodes a series sr to numbers based on the number of classes
        '''
        length = len(sr)
        classes = sr.nunique()
        labels = sr.unique()
        y = np.zeros((length,1))
        for i in range(length):
            for j in range(classes):
                if sr[i] == labels[j]:
                    y[i] = j
        
        return y, classes, labels
        
    def generate_out(self, df, attribute):
        return df[attribute]
        
    #generate y
    def generate_y(self, df, attribute):
        outputs = self.generate_out(df, "Primary Character")
        labels = []
        y, nclasses, labels  = self.encode(outputs)
        m = y.shape[0]
        print(nclasses)
        print(labels)
    
        return y, nclasses, labels, m  
        
    def data_augment(self, X, y):
        for i in range(X.shape[0]):
            xf = np.reshape(tf.image.flip_left_right(X[i]), (1, 224, 224, 3))
            xb = np.reshape(tf.image.random_brightness(X[i], max_delta = 0.5), (1, 224, 224, 3))
            xs = np.reshape(tf.image.random_saturation(X[i], 3, 10), (1, 224, 224, 3))
            xc = np.reshape(tf.image.random_hue(X[i], 0.4), (1, 224, 224, 3))
            X= np.vstack((X, xf, xb, xs, xc))
            y = np.vstack((y, y[i], y[i], y[i], y[i]))
    
            if i % 20 == 0:
                print(X.shape)
        
        print("Final shape:" + str(X.shape))
    
        return X, y
        
    def create_model(self, architecture, nclasses):

        if architecture == 'vgg19':
            base_model = tf.keras.applications.VGG19(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
        elif architecture == 'resnet50':
            base_model = tf.keras.applications.ResNet50V2(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
        elif architecture == 'resnet101':
            base_model = tf.keras.applications.ResNet101V2(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
        elif architecture == 'resnet152':
            base_model = tf.keras.applications.ResNet152V2(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
        elif architecture == 'inception_net':
            base_model = base_model = tf.keras.applications.InceptionV3(include_top = False, pooling = 'max', weights = 'imagenet', 
            input_shape = (224, 224, 3), classes = nclasses)
        elif architecture == 'efficient_net':
            base_model = tf.keras.applications.EfficientNetB7(include_top = False, weights = 'imagenet', 
            input_shape = (224, 224, 3), pooling = 'max', classes = nclasses)

        base_model.trainable = False
        pred_layer = tf.keras.layers.Dense(nclasses, activation = None)
        model = tf.keras.Sequential([
            base_model,
            pred_layer
        ])
        print(model.summary())
        return model
        
    def train(self, model, X_train, y_train, X_dev, y_dev, optimizer, BATCH_SIZE, lr = 0.01, epochs = 30):
        if optimizer == 'Adam':
            opt = tf.keras.optimizers.Adam(learning_rate = lr)
        elif optimizer == 'RMSprop':
            opt = tf.keras.optimizers.RMSprop(learning_rate = lr)
        elif optimizer == 'sgd':
            opt = tf.keras.optimizers.SGD(learning_rate = lr)
        elif optimizer == 'gd_momentum':
            opt = tf.keras.optimizers.SGD(learning_rate = lr, momentum = 0.9)
    
        loss = tf.keras.losses.CategoricalCrossentropy(from_logits = True)
        model.compile(loss = loss, optimizer = opt, metrics = ['accuracy'])
        history = model.fit(x = X_train, y = y_train, validation_data = (X_dev, y_dev), batch_size = BATCH_SIZE, epochs = epochs)
        return history
        
    def plot_metrics(self, model, val):
        fig, axes = plt.subplots(2, sharex = True, figsize = (10,6))
        axes[0].set_ylabel("Loss", fontsize=14)
        axes[1].set_ylabel("Accuracy", fontsize=14)
        axes[1].set_xlabel("Epoch", fontsize=14)

        fig.suptitle('Attribute:' + val)
        axes[0].plot(model.history.history["loss"], label = "Training")
        axes[0].plot(model.history.history["val_loss"], label = "Dev")
        axes[1].plot(model.history.history["accuracy"], label = "Training")
        axes[1].plot(model.history.history["val_accuracy"], label = "Dev")
    


        plt.legend()
        plt.show()
    
    def start(self, run_name, epochs = 30, learning_rate = 0.001, batch_size = 1, architecture = 'vgg19', optimizer = 'Adam'):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            
            Cc = ControllerClient()
            Cc.login("varun.kumar", "Varun@123")
            
            df = pd.read_excel('/data/labels/dataset/labels/carters_data.xlsx', skiprows = 0, header = 1, index_col = None)
            
            X = np.load('/data/X_raw.npy', allow_pickle = True)
            logging.info("X shape: " + str(X.shape))
            
            attribute_names = ['Primary Leg Length', 'Primary Sleeve Length', 'Primary Leg Type', 'Primary Sleeve Type', 'Primary Character', "Print Pattern"]
            model_names = ["leg_length", "sleeve_length", "leg_type", "sleeve_type", "character", "pattern"]
            i = 0
            for attribute in attribute_names:
                y, nclasses, labels, m = self.generate_y(df, attribute)
    
                #Prepare training, dev, and test data
                print("Splitting data")
                X_train_orig, X_test_orig, y_train_orig, y_test_orig = train_test_split(X_d, y_d, test_size = 0.2, random_state = 42)
                X_val_orig, X_test_orig, y_val_orig, y_test_orig = train_test_split(X_test_orig, y_test_orig, test_size = 0.5, random_state = 42)
                
                
                #augment data
                X_train_d, y_train_d = data_augment(X_train_orig, y_train_orig)
                
                X_train = X_train_d/255
                X_test = X_test_orig/255
                X_val = X_val_orig/255

                y_train_d = tf.cast(y_train_d, tf.int32)
                y_train = tf.reshape(tf.one_hot(y_train_d, nclasses), (y_train_d.shape[0],nclasses))
                y_test_orig = tf.cast(y_test_orig, tf.int32)
                y_test = tf.reshape(tf.one_hot(y_test_orig, nclasses), (y_test_orig.shape[0], nclasses))
                y_val_orig = tf.cast(y_val_orig, tf.int32)
                y_val = tf.reshape(tf.one_hot(y_val_orig, nclasses), (y_val_orig.shape[0], nclasses))

                logging.info("y_train shape: " + str(y_train.shape))
                logging.info("y_val shape: " + str(y_val.shape))
                logging.info("y_test shape: " + str(y_test.shape))
    
                BATCH_SIZE = batch_size
                model1 = self.create_model(architecture, nclasses)
                self.train(model1, X_train, y_train, X_val, y_val, optimizer, BATCH_SIZE, lr = learning_rate, epochs = epochs)
                self.plot_metrics(model1, attribute)
                tmp_attribute = attribute.replace(" ", "_")
                self.report_status(status={
                    "status": {
                        "status": f"{attribute} attribute done"
                    },
                    "metric": {
                        f"train_accuracy_{tmp_attribute}": model1.history.history["accuracy"][epochs - 1],
                        f"val_accuracy_{tmp_attribute}": model1.history.history["val_accuracy"][epochs - 1]
                    }
                })
                model1.evaluate(x = X_test, y = y_test)
                model1.save(os.path.join("/data", model_names[i] + '.h5'))
                logging.info(attribute + "done")
                
                #create Output folder
                if not os.path.exists(self.OUTPUT_DIR):
                    os.makedirs(self.OUTPUT_DIR)
                    
                model1.save(os.path.join(self.OUTPUT_DIR, model_names[i] + '.h5'))
                
                logging.info("Model saved")
                i += 1
            
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(push_exp = True, success = True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    #start(self, run_name, epochs = 30, learning_rate = 0.001, batch_size, architecture = 'vgg19', optimizer = 'Adam')
    model_train = Train()
    if len(sys.argv) >= 7:
        # All the arguments are provided as strings
        epochs = int(sys.argv[2])
        learning_rate = float(sys.argv[3])
        batch_size = int(sys.argv[4])
        architecture = sys.argv[5]
        optimizer = sys.argv[6]
        model_train.start(sys.argv[1], epochs, learning_rate, batch_size, architecture, optimizer)
    else:
        model_train.start(run_name="")
