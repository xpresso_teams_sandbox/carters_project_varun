MOUNT_PATH = '/data'
import os
import types
import pickle
import marshal
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot


## $xpr_param_component_name = train
## $xpr_param_component_type = pipeline_job

df = pd.read_excel('/data/carters_data.xlsx', skiprows = 0, header = 1, index_col = None)
def encode(sr):
    ''' (Series) --> np.array
    Encodes a series sr to numbers based on the number of classes
    '''
    length = len(sr)
    classes = sr.nunique()
    labels = sr.unique()
    y = np.zeros((length,1))
    for i in range(length):
        for j in range(classes):
            if sr[i] == labels[j]:
                y[i] = j
        
    return y, classes, labels
    
def generate_out(df, attribute):
    return df[attribute]
    
#generate y
def generate_y(attribute):
    outputs = generate_out(df, "Primary Character")
    labels = []
    y, nclasses, labels  = encode(outputs)
    m = y.shape[0]
    print(nclasses)
    print(labels)
    
    return y, nclasses, labels, m
    
# load X data and create y
X = np.load('/data/X.npy')
print("X shape: " + str(X.shape))

def data_augment(X, y):
    for i in range(X.shape[0]):
        xf = np.reshape(tf.image.flip_left_right(X[i]), (1, 224, 224, 3))
        xb = np.reshape(tf.image.random_brightness(X[i], max_delta = 0.5), (1, 224, 224, 3))
        xs = np.reshape(tf.image.random_saturation(X[i], 3, 10), (1, 224, 224, 3))
        xc = np.reshape(tf.image.random_hue(X[i], 0.4), (1, 224, 224, 3))
        X= np.vstack((X, xf, xb, xs, xc))
        y = np.vstack((y, y[i], y[i], y[i], y[i]))
    
        if i % 20 == 0:
            print(X.shape)
        
    print("Final shape:" + str(X.shape))
    
    return X, y
    
def create_model(architecture):

    if architecture == 'vgg19':
        base_model = tf.keras.applications.VGG19(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
    elif architecture == 'resnet50':
        base_model = tf.keras.applications.ResNet50V2(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
    elif architecture == 'resnet101':
        base_model = tf.keras.applications.ResNet101V2(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
    elif architecture == 'resnet152':
        base_model = tf.keras.applications.ResNet152V2(include_top = False, pooling = 'max', weights = 'imagenet', classes = nclasses)
    elif architecture == 'inception_net':
        base_model = base_model = tf.keras.applications.InceptionV3(include_top = False, pooling = 'max', weights = 'imagenet', 
        input_shape = (224, 224, 3), classes = nclasses)
    elif architecture == 'efficient_net':
        base_model = tf.keras.applications.EfficientNetB7(include_top = False, weights = 'imagenet', 
        input_shape = (224, 224, 3), pooling = 'max', classes = nclasses)

    base_model.trainable = False
    pred_layer = tf.keras.layers.Dense(nclasses, activation = None)
    model = tf.keras.Sequential([
        base_model,
        pred_layer
    ])
    print(model.summary())
    return model
    
    
def train(model, X_train, y_train, X_dev, y_dev, optimizer, BATCH_SIZE, lr = 0.01, epochs = 30):
    if optimizer == 'Adam':
        opt = tf.keras.optimizers.Adam(learning_rate = lr)
    elif optimizer == 'RMSprop':
        opt = tf.keras.optimizers.RMSprop(learning_rate = lr)
    elif optimizer == 'sgd':
        opt = tf.keras.optimizers.SGD(learning_rate = lr)
    elif optimizer == 'gd_momentum':
        opt = tf.keras.optimizers.SGD(learning_rate = lr, momentum = 0.9)
    
    loss = tf.keras.losses.CategoricalCrossentropy(from_logits = True)
    model.compile(loss = loss, optimizer = opt, metrics = ['accuracy'])
    history = model.fit(x = X_train, y = y_train, validation_data = (X_dev, y_dev), batch_size = BATCH_SIZE, epochs = epochs)
    return history
    
    
def plot_metrics(model, val):
    fig, axes = plt.subplots(2, sharex = True, figsize = (10,6))
    axes[0].set_ylabel("Loss", fontsize=14)
    axes[1].set_ylabel("Accuracy", fontsize=14)
    axes[1].set_xlabel("Epoch", fontsize=14)

    fig.suptitle('Attribute:' + val)
    axes[0].plot(model.history.history["loss"], label = "Training")
    axes[0].plot(model.history.history["val_loss"], label = "Dev")
    axes[1].plot(model.history.history["accuracy"], label = "Training")
    axes[1].plot(model.history.history["val_accuracy"], label = "Dev")
    


    plt.legend()
    plt.show()
    
    
attribute_names = ['Primary Leg Length', 'Primary Sleeve Length', 'Primary Leg Type', 'Primary Sleeve Type', 'Primary Character', "Print Pattern"]

for attribute in attribute_names:
    y, nclasses, labels, m = generate_y(attribute)
    
    #augment data
    X_d, y_d = data_augment(X, y)
    
    #Prepare training, dev, and test data
    print("Splitting data")
    X_train_orig, X_test_orig, y_train_orig, y_test_orig = train_test_split(X_d, y_d, test_size = 0.2, random_state = 42)
    X_val_orig, X_test_orig, y_val_orig, y_test_orig = train_test_split(X_test_orig, y_test_orig, test_size = 0.5, random_state = 42)
    
    X_train = X_train_orig/255
    X_test = X_test_orig/255
    X_val = X_val_orig/255

    y_train_orig = tf.cast(y_train_orig, tf.int32)
    y_train = tf.reshape(tf.one_hot(y_train_orig, nclasses), (996,nclasses))
    y_test_orig = tf.cast(y_test_orig, tf.int32)
    y_test = tf.reshape(tf.one_hot(y_test_orig, nclasses), (125, nclasses))
    y_val_orig = tf.cast(y_val_orig, tf.int32)
    y_val = tf.reshape(tf.one_hot(y_val_orig, nclasses), (124, nclasses))

    print("y_train shape: " + str(y_train.shape))
    print("y_val shape: " + str(y_val.shape))
    print("y_test shape: " + str(y_test.shape))
    
    BATCH_SIZE = 1
    model1 = create_model('vgg19')
    train(model1, X_train, y_train, X_val, y_val, 'Adam', BATCH_SIZE, lr = 0.001, epochs = 40)
    plot_metrics(model1, attribute)
    model1.evaluate(x = X_test, y = y_test)
    model1.save(attribute + '.h5')
    

